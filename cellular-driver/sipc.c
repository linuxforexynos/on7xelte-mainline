#pragma GCC diagnostic ignored "-Wdeclaration-after-statement"

#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/netdevice.h>
#include <linux/platform_device.h>
#include <linux/mod_devicetable.h>
#include <linux/libnvdimm.h>
#include <linux/regmap.h>
#include <linux/mfd/syscon.h>
#include <linux/of.h>
#include <linux/ioctl.h>
#include <linux/poll.h>
#include <sound/core.h> //copy_from_user_toio

#include "sipc.h"

//a=0x82000700, b={3,4} should probably go to the pmu driver
//(they should, but drivers are looking up its regmap directly, so it's a no-go)
uint64_t exynos_smc(uint64_t a, uint64_t b, uint64_t c, uint64_t d);

asm(".section .text\nexynos_smc:\nsmc #0\nret");

#define IOCTL_MODEM_ON        _IO('o', 0x19) //0x6f19
#define IOCTL_MODEM_RESET     _IO('o', 0x21) //0x6f21
#define IOCTL_MODEM_BOOT_ON   _IO('o', 0x22) //0x6f22
#define IOCTL_MODEM_BOOT_OFF  _IO('o', 0x23) //0x6f23
#define IOCTL_MODEM_STATUS    _IO('o', 0x27) //0x6f27
#define IOCTL_MODEM_DL_START  _IO('o', 0x28) //0x6f28
#define IOCTL_DPRAM_SEND_BOOT _IO('o', 0x40) //0x6f40
#define IOCTL_SECURITY_REQ    _IO('o', 0x53) //0x6f53

struct __attribute__((__packed__)) security_req {
    uint32_t mode;
    uint32_t size_boot;
    uint32_t size_main;
    uint32_t pad_zero;
};

struct __attribute__((__packed__)) modem_firmware_partition_data {
    uint8_t *binary;
    uint32_t size;
    uint32_t m_offset;
    uint32_t b_offset;
    uint32_t mode;
    ssize_t len;
};

static const struct ring_buffer fmt_tx = {0x8, 0xc, 0x1000, 0x1000};
static const struct ring_buffer fmt_rx = {0x10, 0x14, 0x2000, 0x1000};
static const struct ring_buffer raw_tx = {0x18, 0x1c, 0x3000, 0x1fd000};
static const struct ring_buffer raw_rx = {0x20, 0x24, 0x200000, 0x200000};

static bool change_state(struct sipc_device* dev, int old_state, int new_state)
{
    return atomic_try_cmpxchg(&dev->modem_state, &old_state, new_state);
}

static void set_state(struct sipc_device* dev, int new_state)
{
    atomic_set(&dev->modem_state, new_state);
}

static int get_state(struct sipc_device* dev)
{
    int ans = atomic_read(&dev->modem_state);
    return ans;
}

static bool can_do_ipc(struct sipc_device* dev)
{
    int state = get_state(dev);
    return state == MODEM_STATE_DL_STARTED || state == MODEM_STATE_BOOTED_OFF;
}

static void lock_ipc(struct sipc_device* dev)
{
    mutex_lock(&dev->raw_tx_mtx);
    mutex_lock(&dev->fmt_tx_mtx);
    mutex_lock(&dev->irq_mtx);
}

static void unlock_ipc(struct sipc_device* dev)
{
    mutex_unlock(&dev->irq_mtx);
    mutex_unlock(&dev->fmt_tx_mtx);
    mutex_unlock(&dev->raw_tx_mtx);
}

static void packet_list_push(struct packet_list* lst, struct packet_item* item)
{
    pr_debug("packet_list_push, size = 0x%lx\n", item->size);
    item->next = NULL;
    mutex_lock(&lst->mtx);
    if(item->size > lst->room_left)
        kfree(item);
    else
    {
        *lst->head = item;
        lst->head = &item->next;
        lst->room_left -= item->size;
    }
    mutex_unlock(&lst->mtx);
}

static struct packet_item* packet_list_pop(struct packet_list* lst)
{
    mutex_lock(&lst->mtx);
    struct packet_item* ans = lst->tail;
    if(ans)
    {
        lst->tail = ans->next;
        if(!lst->tail)
            lst->head = &lst->tail;
        lst->room_left += ans->size;
    }
    mutex_unlock(&lst->mtx);
    return ans;
}

static int packet_list_empty(struct packet_list* lst)
{
    mutex_lock(&lst->mtx);
    int ans = !lst->tail;
    mutex_unlock(&lst->mtx);
    return ans;
}

static irqreturn_t atomic_handler(int irq, void* opaque)
{
    struct sipc_device* dev = opaque;
    pr_debug("in atomic_handler\n");
    uint32_t interrupts = readl(dev->mcu_regs+0x14);
    if(interrupts)
    {
        pr_debug("interrupts = 0x%x\n", interrupts);
        writel(interrupts, dev->mcu_regs+0xc);
        if(interrupts & (1 << 16))
        {
            uint32_t msg = readl(dev->mcu_regs+0x84);
            pr_debug("cmd = 0x%x\n", msg);
            if(msg == 0xc8)
            {
                atomic_set(&dev->had_cmd8, 1);
                wake_up(&dev->irq_waitqueue);
            }
            else if(msg >= 0x80 && msg < 0xc0 && (msg & 3))
                return IRQ_WAKE_THREAD;
        }
    }
    return IRQ_HANDLED;
}

static ssize_t write_buf(struct sipc_device* dev, struct file* f, const struct ring_buffer* buf, struct mutex* mtx, const void* header, size_t header_size, const void* data, size_t data_size)
{
    size_t orig_data_size = data_size;
    size_t sz = header_size + data_size;
    if(sz >= buf->size)
        return -E2BIG;
    DEFINE_WAIT(wq_entry);
    for(;;)
    {
        prepare_to_wait(&dev->irq_waitqueue, &wq_entry, TASK_INTERRUPTIBLE);
        if(signal_pending(current))
        {
            finish_wait(&dev->irq_waitqueue, &wq_entry);
            return -ERESTARTSYS;
        }
        mutex_lock(mtx);
        if(!can_do_ipc(dev))
        {
            mutex_unlock(mtx);
            finish_wait(&dev->irq_waitqueue, &wq_entry);
            return -EBADFD;
        }
        uint32_t head = readl(dev->ipc_mapping+buf->head_offset);
        uint32_t tail = readl(dev->ipc_mapping+buf->tail_offset);
        uint32_t effective_tail = tail;
        if(effective_tail <= head)
            effective_tail += buf->size;
        if(effective_tail - head <= sz)
        {
            mutex_unlock(mtx);
            if(!f || (f->f_flags & O_NONBLOCK))
            {
                finish_wait(&dev->irq_waitqueue, &wq_entry);
                return -EAGAIN;
            }
            schedule();
            continue;
        }
        finish_wait(&dev->irq_waitqueue, &wq_entry);
        while(header_size)
        {
            uint32_t cp = buf->size - head;
            if(likely(cp > header_size))
                cp = header_size;
            memcpy_toio(dev->ipc_mapping+buf->start+head, header, cp);
            head += cp;
            header += cp;
            header_size -= cp;
            if(head == buf->size)
                head = 0;
        }
        while(data_size)
        {
            uint32_t cp = buf->size - head;
            if(likely(cp > data_size))
                cp = data_size;
            if(!f)
                memcpy_toio(dev->ipc_mapping+buf->start+head, data, cp);
            else if(copy_from_user_toio(dev->ipc_mapping+buf->start+head, data, cp))
            {
                mutex_unlock(mtx);
                return -EFAULT;
            }
            head += cp;
            data += cp;
            data_size -= cp;
            if(head == buf->size)
                head = 0;
        }
        head = ((head + 7) | 7) - 7;
        smp_mb();
        writel(head, dev->ipc_mapping+buf->head_offset);
        smp_mb();
        writel(0x83, dev->mcu_regs+0x80);
        writel(1, dev->mcu_regs+0x1c);
        mutex_unlock(mtx);
        return orig_data_size;
    }
}

static void poll_buf(struct sipc_device* dev, const struct ring_buffer* buf, const struct protocol_map* protocols, int n_protocols)
{
    //XXX: should we abort if the modem gives us invalid offsets?
    uint32_t head = readl(dev->ipc_mapping+buf->head_offset) % buf->size;
    uint32_t tail = readl(dev->ipc_mapping+buf->tail_offset) % buf->size;
    uint32_t sz = head - tail; //type is important, integer overflow happens here
    if(head < tail)
        sz += buf->size;
    pr_debug("poll_buf: head = 0x%x, tail = 0x%x, sz = 0x%x\n", head, tail, sz);
    while(sz >= 4)
    {
        uint16_t header[2];
        if(unlikely(buf->size - tail < 4))
        {
            void* mem = header;
            memcpy_fromio(mem, dev->ipc_mapping+buf->start+tail, buf->size-tail);
            memcpy_fromio(mem+(buf->size-tail), dev->ipc_mapping+buf->start, 4-(buf->size-tail));
        }
        else
            memcpy_fromio(header, dev->ipc_mapping+buf->start+tail, 4);
        uint32_t size = header[1];
        uint32_t padded_size = ((size + 7) | 7) - 7;
        pr_debug("packet size 0x%x, padded 0x%x\n", size, padded_size);
        if(size >= buf->size) //invalid packet size
        {
            pr_err("packet too big, stopping rx\n");
            break;
        }
        if(padded_size > sz) //packet has not fully arrived. should not happen
        {
            pr_debug("partial packet, skipping\n");
            break;
        }
        uint32_t new_tail = tail + size;
        if(new_tail >= buf->size)
            new_tail -= buf->size;
        const struct protocol_map* proto = NULL;
        for(int i = 0; i < n_protocols; i++)
            if(protocols[i].protocol == header[0])
                proto = &protocols[i];
        pr_debug("protocol 0x%x (%sknown)\n", header[0], proto?"":"un");
        if(proto)
        {
            uint32_t copy_start = tail + 4;
            if(copy_start >= buf->size)
                copy_start -= buf->size;
            uint32_t copy_size = size - 4;
            char* pkt = NULL;
            struct sk_buff* skb = NULL;
            struct packet_item* item;
            if(proto->queue == PACKET_LIST_IP)
            {
                skb = dev_alloc_skb(copy_size+2);
                pkt = skb_put(skb, copy_size);
            }
            else
            {
                item = kmalloc(sizeof(*item) + copy_size, GFP_KERNEL);
                pkt = item->data;
            }
            if(copy_start > new_tail)
            {
                memcpy_fromio(pkt, dev->ipc_mapping+buf->start+copy_start, buf->size-copy_start);
                memcpy_fromio(pkt+(buf->size-copy_start), dev->ipc_mapping+buf->start, new_tail);
            }
            else
                memcpy_fromio(pkt, dev->ipc_mapping+buf->start+copy_start, new_tail-copy_start);
            if(proto->queue == PACKET_LIST_IP)
            {
                skb->dev = dev->rmnet0;
                if(((uint8_t)pkt[0] & 0xf0) == 0x60)
                    skb->protocol = 0xdd86; //IPv6
                else
                    skb->protocol = 8; //IPv4
                pr_debug("got to netif_rx\n");
                netif_rx(skb);
            }
            else
            {
                item->size = copy_size;
                packet_list_push(proto->queue, item);
            }
        }
        tail = ((new_tail + 7) | 7) - 7;
        /* aligning here can result in the past-the-end offset if we're a few bytes behind the end of the buffer */
        if(tail >= buf->size)
            tail -= buf->size;
        sz -= padded_size;
    }
    pr_debug("writing tail=0x%x\n", tail);
    writel(tail, dev->ipc_mapping+buf->tail_offset);
}

static irqreturn_t threaded_handler(int irq, void* opaque)
{
    struct sipc_device* dev = opaque;
    pr_debug("in threaded_handler\n");
    mutex_lock(&dev->irq_mtx);
    if(!can_do_ipc(dev))
    {
        mutex_unlock(&dev->irq_mtx);
        return IRQ_HANDLED;
    }
    struct protocol_map raw_protos[3] = {{0xd7f8, &dev->boot0_rx}, {0xf5fc, &dev->rfs0_rx}, {0x0afc, PACKET_LIST_IP}};
    poll_buf(dev, &raw_rx, raw_protos, 3);
    struct protocol_map fmt_protos[1] = {{0xebfc, &dev->ipc0_rx}};
    poll_buf(dev, &fmt_rx, fmt_protos, 1);
    mutex_unlock(&dev->irq_mtx);
    pr_debug("waking up waiters\n");
    wake_up(&dev->irq_waitqueue);
    return IRQ_HANDLED;
}

static void sipc_do_reset(struct sipc_device* dev)
{
    uint64_t cp_state = exynos_smc(0x82000700, 3, 0, 1) >> 16;
    if((cp_state & 2))
    {
        regmap_write(dev->pmu, 0x280, 0);
        regmap_write(dev->pmu, 0x1170, 0);
        regmap_write(dev->pmu, 0x11d0, 0);
        regmap_write(dev->pmu, 0x11d8, 0);
        regmap_write(dev->pmu, 0x11d4, 0);
        regmap_write(dev->pmu, 0x11cc, 0);
        cp_state |= 4;
        exynos_smc(0x82000700, 4, cp_state, 1);
    }
}

static long boot0_ioctl(struct file* f, unsigned int command, unsigned long arg)
{
    struct sipc_device* dev = container_of(f->private_data, struct sipc_device, umts_boot0);
    switch(command)
    {
#if 0
    case 0x13371337:
    {
        uint64_t args[4];
        copy_from_user(args, (void __user*)arg, sizeof(args));
        args[0] = exynos_smc(args[0], args[1], args[2], args[3]);
        copy_to_user((void __user*)arg, args, sizeof(args));
        return 0;
    }
#endif
    case IOCTL_MODEM_RESET:
    {
        lock_ipc(dev);
        mutex_lock(&dev->fw_mtx);
        int old_state = atomic_xchg(&dev->modem_state, MODEM_STATE_RESETTING);
        mutex_unlock(&dev->fw_mtx);
        unlock_ipc(dev);
        if(old_state == MODEM_STATE_RESETTING)
            return -EBADFD;
        else if(old_state == MODEM_STATE_OFFLINE)
        {
            set_state(dev, MODEM_STATE_OFFLINE);
            return -EBADFD;
        }
        sipc_do_reset(dev);
        atomic_set(&dev->had_cmd8, 0);
        set_state(dev, MODEM_STATE_RESET);
        return 0;
    }
    case IOCTL_SECURITY_REQ:
    {
        struct security_req req;
        if(copy_from_user(&req, (void __user*)arg, sizeof(req)))
            return -EFAULT;
        if(req.mode != 0 && req.mode != 2)
            return -EINVAL;
        if(req.mode == 2 && (req.size_boot || req.size_main))
            return -EINVAL;
        if(req.mode == 0)
        {
            mutex_lock(&dev->fw_mtx);
            bool ok = change_state(dev, MODEM_STATE_UNLOCKED_MEMORY, MODEM_STATE_LOCKING_MEMORY);
            mutex_unlock(&dev->fw_mtx);
            if(!ok)
                return -EBADFD;
            smp_mb();
        }
        if(req.mode == 2 && !change_state(dev, MODEM_STATE_RESET, MODEM_STATE_UNLOCKING_MEMORY))
            return -EBADFD;
        exynos_smc(0x82001011, 0, 0, 0);
        uint32_t request_status = exynos_smc(0x82000700, req.mode, req.size_boot, req.size_main);
        exynos_smc(0x82001011, 1, 0, 0);
        if(request_status != 0 && request_status != 11)
        {
            //the mode switch did not succeed. probably the signature check has failed
            set_state(dev, MODEM_STATE_RESET);
            return -ENOEXEC;
        }
        if(req.mode == 2)
        {
            memset_io(dev->firmware_mapping, 0, 0x7400000);
            set_state(dev, MODEM_STATE_UNLOCKED_MEMORY);
        }
        else
            set_state(dev, MODEM_STATE_LOCKED_MEMORY);
        return 0;
    }
    case IOCTL_DPRAM_SEND_BOOT:
    {
        struct modem_firmware_partition_data req;
        if(copy_from_user(&req, (void __user*)arg, sizeof(req)))
            return -EFAULT;
        size_t shmem_offset = req.m_offset;
        void __user* user_addr = (void __user*)req.binary;
        size_t size = req.len;
        if(shmem_offset > dev->firmware_size || size > dev->firmware_size - shmem_offset)
            return -ERANGE;
        mutex_lock(&dev->fw_mtx);
        int state = get_state(dev);
        if(state != MODEM_STATE_UNLOCKED_MEMORY)
        {
            mutex_unlock(&dev->fw_mtx);
            return -EBADFD;
        }
        if(copy_from_user_toio(dev->firmware_mapping+shmem_offset, user_addr, size))
            return -EFAULT;
        mutex_unlock(&dev->fw_mtx);
        return 0;
    }
    case IOCTL_MODEM_ON:
    {
        if(!change_state(dev, MODEM_STATE_LOCKED_MEMORY, MODEM_STATE_TURNING_ON))
            return -EBADFD;
        size_t i;
        pr_err("0xa4 = 0x%x\n", readl(dev->mcu_regs+0xa4));
        pr_err("0x94 = 0x%x\n", readl(dev->mcu_regs+0x94));
        for(i = 0x80; i < 0x180; i += 4)
            writel(0, dev->mcu_regs+i);
        writel(4, dev->mcu_regs+0x104);
        int version = readl(dev->mcu_regs+0x104);
        if(version != 4)
        {
            pr_err("unsupported version reported: %d\n", version);
            set_state(dev, MODEM_STATE_DEFUNCT);
            return -EINVAL;
        }
        writel(1, dev->mcu_regs+0x98);
        //TODO: magic writes with unknown original logic
        //exynos_smc(0x82000700, 3, 0, 1);
        //exynos_smc(0x82000700, 3, 0, 1);
        int reg = exynos_smc(0x82000700, 3, 0, 1);
        if(!(reg & 0x20000))
            exynos_smc(0x82000700, 4, (reg >> 16) | 2, 1);
        //exynos_smc(0x82000700, 3, 0, 1);
        //exynos_smc(0x82000700, 3, 0, 0);
        exynos_smc(0x82000700, 4, 8, 0);
        //exynos_smc(0x82000700, 3, 0, 0);
        set_state(dev, MODEM_STATE_TURNED_ON);
        return 0;
    }
    case IOCTL_MODEM_BOOT_ON:
    {
        if(!change_state(dev, MODEM_STATE_TURNED_ON, MODEM_STATE_BOOTING_ON))
            return -EBADFD;
        pr_err("waiting for 0x94=1\n");
        while(!signal_pending(current) && readl(dev->mcu_regs+0x94) != 1);
        if(signal_pending(current))
        {
            set_state(dev, MODEM_STATE_TURNED_ON);
            return -ERESTARTSYS;
        }
        else
        {
            set_state(dev, MODEM_STATE_BOOTED_ON);
            return 0;
        }
    }
    case IOCTL_MODEM_DL_START:
    {
        if(!change_state(dev, MODEM_STATE_BOOTED_ON, MODEM_STATE_DL_STARTING))
            return -EBADFD;
        size_t i;
        for(i = 8; i < 0x28; i += 4)
            writel(0, dev->ipc_mapping+i);
        writel(0x424f4f54, dev->ipc_mapping); //'BOOT'
        pr_err("*0xf7400000 = 0x%x\n", readl(dev->ipc_mapping));
        smp_mb();
        writel(1, dev->mcu_regs+0x90);
        pr_err("0x90 = 0x%x\n", readl(dev->mcu_regs+0x90));
        set_state(dev, MODEM_STATE_DL_STARTED);
        threaded_handler(0, dev);
        return 0;
    }
    case IOCTL_MODEM_BOOT_OFF:
    {
        lock_ipc(dev);
        bool ok = change_state(dev, MODEM_STATE_DL_STARTED, MODEM_STATE_BOOTING_OFF);
        unlock_ipc(dev);
        if(!ok)
            return -EBADFD;
        wait_event_interruptible(dev->irq_waitqueue, atomic_read(&dev->had_cmd8));
        if(signal_pending(current))
        {
            set_state(dev, MODEM_STATE_DL_STARTED);
            return -ERESTARTSYS;
        }
        writel(0, dev->ipc_mapping+4);
        writel(0, dev->ipc_mapping);
        for(size_t i = 8; i < 0x28; i++)
            writel(0, dev->ipc_mapping);
        writel(0xaa, dev->ipc_mapping);
        writel(1, dev->ipc_mapping+4);
        smp_mb();
        writel(0xc2, dev->mcu_regs+0x80);
        writel(1, dev->mcu_regs+0x1c);
        set_state(dev, MODEM_STATE_BOOTED_OFF);
        threaded_handler(0, dev);
        return 0;
    }
    case IOCTL_MODEM_STATUS:
        return (get_state(dev) == MODEM_STATE_BOOTED_OFF && atomic_read(&dev->had_cmd8)) ? 4 : 0;
    default:
        return -ENOTTY;
    }
}

static ssize_t packet_list_read(struct sipc_device* dev, struct file* f, struct packet_list* lst, char __user* data, size_t size)
{
    struct packet_item* item = packet_list_pop(lst);
    if(!item)
    {
        if((f->f_flags & O_NONBLOCK))
            return -EAGAIN;
        wait_event_interruptible(dev->irq_waitqueue, (item = packet_list_pop(lst)));
        if(signal_pending(current))
            return -ERESTARTSYS;
    }
    BUG_ON(!item);
    if(item->size < size)
        size = item->size;
    int err = copy_to_user(data, item->data, size);
    kfree(item);
    return err ? -EFAULT : (ssize_t)size;
}

static unsigned int channel_poll(struct sipc_device* dev, struct file* f, const struct ring_buffer* tx, struct mutex* tx_mtx, struct packet_list* rx, poll_table* wait)
{
    poll_wait(f, &dev->irq_waitqueue, wait);
    int ans = 0;
    if(!packet_list_empty(rx))
        ans |= POLLIN | POLLRDNORM;
    mutex_lock(tx_mtx);
    if(!can_do_ipc(dev))
    {
        mutex_unlock(tx_mtx);
        return ans;
    }
    uint32_t head = readl(dev->ipc_mapping+tx->head_offset);
    uint32_t tail = readl(dev->ipc_mapping+tx->tail_offset);
    mutex_unlock(tx_mtx);
    if(head < tail)
        head += tx->size;
    if(tx->size - (head - tail) >= 65536)
        ans |= POLLOUT | POLLWRNORM;
    pr_debug("channel_poll: returning %d\n", ans);
    return ans;
}

static ssize_t boot0_write(struct file* f, const char __user* data, size_t size, loff_t* off)
{
    struct sipc_device* dev = container_of(f->private_data, struct sipc_device, umts_boot0);
    if(size >= 0x10000 - 4 || size >= raw_tx.size - 4)
        return -E2BIG;
    uint16_t header[2] = {0xd7f8, size+4};
    return write_buf(dev, f, &raw_tx, &dev->raw_tx_mtx, header, sizeof(header), data, size);
}

static ssize_t boot0_read(struct file* f, char __user* data, size_t size, loff_t* off)
{
    struct sipc_device* dev = container_of(f->private_data, struct sipc_device, umts_boot0);
    return packet_list_read(dev, f, &dev->boot0_rx, data, size);
}

static unsigned int boot0_poll(struct file* f, poll_table* wait)
{
    struct sipc_device* dev = container_of(f->private_data, struct sipc_device, umts_boot0);
    return channel_poll(dev, f, &raw_tx, &dev->raw_tx_mtx, &dev->boot0_rx, wait);
}

static const struct file_operations boot0_fops = {
    .read = boot0_read,
    .write = boot0_write,
    .poll = boot0_poll,
    .unlocked_ioctl = boot0_ioctl,
};

static ssize_t ipc0_write(struct file* f, const char __user* data, size_t size, loff_t* off)
{
    struct sipc_device* dev = container_of(f->private_data, struct sipc_device, umts_ipc0);
    if(size >= 0x10000 - 4 || size >= raw_tx.size - 4)
        return -E2BIG;
    uint16_t header[2] = {0xebfc, size+4};
    return write_buf(dev, f, &fmt_tx, &dev->fmt_tx_mtx, header, sizeof(header), data, size);
}

static ssize_t ipc0_read(struct file* f, char __user* data, size_t size, loff_t* off)
{
    struct sipc_device* dev = container_of(f->private_data, struct sipc_device, umts_ipc0);
    return packet_list_read(dev, f, &dev->ipc0_rx, data, size);
}

static unsigned int ipc0_poll(struct file* f, poll_table* wait)
{
    struct sipc_device* dev = container_of(f->private_data, struct sipc_device, umts_ipc0);
    return channel_poll(dev, f, &fmt_tx, &dev->fmt_tx_mtx, &dev->ipc0_rx, wait);
}

static long ipc0_ioctl(struct file* f, unsigned int command, unsigned long arg)
{
    struct sipc_device* dev = container_of(f->private_data, struct sipc_device, umts_ipc0);
    switch(command)
    {
    case IOCTL_MODEM_STATUS:
        return atomic_read(&dev->had_cmd8) ? 4 : 0;
    default:
        return -ENOTTY;
    }
}

static const struct file_operations ipc0_fops = {
    .read = ipc0_read,
    .write = ipc0_write,
    .poll = ipc0_poll,
    .unlocked_ioctl = ipc0_ioctl,
};

static ssize_t rfs0_write(struct file* f, const char __user* data, size_t size, loff_t* off)
{
    struct sipc_device* dev = container_of(f->private_data, struct sipc_device, umts_rfs0);
    if(size >= 0x10000 - 4 || size >= raw_tx.size - 4)
        return -E2BIG;
    uint16_t header[2] = {0xf5fc, size+4};
    return write_buf(dev, f, &raw_tx, &dev->raw_tx_mtx, header, sizeof(header), data, size);
}

static ssize_t rfs0_read(struct file* f, char __user* data, size_t size, loff_t* off)
{
    struct sipc_device* dev = container_of(f->private_data, struct sipc_device, umts_rfs0);
    return packet_list_read(dev, f, &dev->rfs0_rx, data, size);
}

static unsigned int rfs0_poll(struct file* f, poll_table* wait)
{
    struct sipc_device* dev = container_of(f->private_data, struct sipc_device, umts_rfs0);
    return channel_poll(dev, f, &raw_tx, &dev->raw_tx_mtx, &dev->rfs0_rx, wait);
}

static long rfs0_ioctl(struct file* f, unsigned int command, unsigned long arg)
{
    struct sipc_device* dev = container_of(f->private_data, struct sipc_device, umts_rfs0);
    switch(command)
    {
    case IOCTL_MODEM_STATUS:
        return atomic_read(&dev->had_cmd8) ? 4 : 0;
    default:
        return -ENOTTY;
    }
}

static const struct file_operations rfs0_fops = {
    .read = rfs0_read,
    .write = rfs0_write,
    .poll = rfs0_poll,
    .unlocked_ioctl = rfs0_ioctl,
};

static void rmnet_setup(struct net_device* dev){}

static netdev_tx_t rmnet_xmit(struct sk_buff* skb, struct net_device* netdev)
{
    struct sipc_device* dev = *(struct sipc_device**)netdev_priv(netdev);
    char* data = skb->data;
    int len = skb->len;
    int protocol = skb->protocol;
    if(len < 65532)
    {
        uint16_t header[2];
        header[1] = (uint16_t)len + 4;
        if(protocol == 8 || protocol == 0xdd86) //IPv4 or IPv6
        {
            header[0] = 0x0afc;
            //should succeed. if it fails, then the packet just got dropped, that's ok
            write_buf(dev, NULL, &raw_tx, &dev->raw_tx_mtx, header, sizeof(header), data, len);
        }
    }
    kfree_skb(skb);
    return NETDEV_TX_OK;
}

static const struct net_device_ops rmnet_ops = {
    .ndo_start_xmit = rmnet_xmit,
};

static void rx_init(struct packet_list* lst, size_t buffer_size)
{
    mutex_init(&lst->mtx);
    lst->tail = NULL;
    lst->head = &lst->tail;
    lst->room_left = buffer_size;
}

static void rx_destroy(struct packet_list* lst)
{
    struct packet_item* item;
    while((item = packet_list_pop(lst)))
        kfree(item);
    mutex_destroy(&lst->mtx);
}

static int do_misc_register(struct device* parent, struct miscdevice* dev, const char* name, const struct file_operations* fops)
{
    memset(dev, 0, sizeof(*dev));
    dev->minor = MISC_DYNAMIC_MINOR;
    dev->name = name;
    dev->fops = fops;
    int err = misc_register(dev);
    if(err)
        dev_err(parent, "failed to register %s: %d\n", name, err);
    return err;
}

static int sipc_probe(struct platform_device* pdev)
{
    int err;
    struct sipc_device* dev = devm_kmalloc(&pdev->dev, sizeof(*dev), GFP_KERNEL);
    set_state(dev, MODEM_STATE_OFFLINE);
    dev->mcu_regs = devm_ioremap_resource(&pdev->dev, &pdev->resource[0]);
    if(!dev->mcu_regs || IS_ERR(dev->mcu_regs))
    {
        dev_err(&pdev->dev, "failed to map MCU registers: %d\n", (int)PTR_ERR(dev->mcu_regs));
        return PTR_ERR(dev->mcu_regs);
    }
    dev->firmware_size = pdev->resource[1].end - pdev->resource[1].start;
    dev->firmware_mapping = ioremap_wc(pdev->resource[1].start, dev->firmware_size);
    if(!dev->firmware_mapping || IS_ERR(dev->firmware_mapping))
    {
        dev_err(&pdev->dev, "failed to map backing memory for firmware: %d\n", (int)PTR_ERR(dev->firmware_mapping));
        return PTR_ERR(dev->firmware_mapping);
    }
    mutex_init(&dev->raw_tx_mtx);
    mutex_init(&dev->fmt_tx_mtx);
    mutex_init(&dev->irq_mtx);
    mutex_init(&dev->fw_mtx);
    rx_init(&dev->boot0_rx, 262144);
    rx_init(&dev->ipc0_rx, 131072);
    rx_init(&dev->rfs0_rx, 131072);
    dev->ipc_mapping = ioremap_wc(pdev->resource[2].start, 0x400000);
    if(!dev->ipc_mapping || IS_ERR(dev->ipc_mapping))
    {
        err = PTR_ERR(dev->ipc_mapping);
        dev_err(&pdev->dev, "failed to map IPC memory: %d\n", err);
        goto cleanup1;
    }
    dev_dbg(&pdev->dev, "addresses: 0x%llx 0x%llx 0x%llx\n", page_to_phys(vmalloc_to_page((void*)dev->mcu_regs)), page_to_phys(vmalloc_to_page((void*)dev->firmware_mapping)), page_to_phys(vmalloc_to_page((void*)dev->ipc_mapping)));
    //writel(0xffff, dev->mcu_regs+0x20);
    dev->pmu = syscon_regmap_lookup_by_phandle(pdev->dev.of_node, "samsung,pmu-syscon");
    if(IS_ERR(dev->pmu))
    {
        err = PTR_ERR(dev->pmu);
        dev_err(&pdev->dev, "failed to look up PMU regmap: %d\n", err);
        goto cleanup2;
    }
    struct pinctrl* pinctrl = devm_pinctrl_get(&pdev->dev);
    if(IS_ERR(pinctrl))
    {
        err = PTR_ERR(pinctrl);
        dev_err(&pdev->dev, "failed to get pinctrl: %d\n", err);
        goto cleanup2;
    }
    struct pinctrl_state* pinctrl_default = pinctrl_lookup_state(pinctrl, PINCTRL_STATE_DEFAULT);
    if(IS_ERR(pinctrl_default))
    {
        err = PTR_ERR(pinctrl_default);
        dev_err(&pdev->dev, "failed to lookup pinctrl default state: %d\n", err);
        goto cleanup2;
    }
    err = pinctrl_select_state(pinctrl, pinctrl_default);
    if(err)
    {
        dev_err(&pdev->dev, "failed to set pinctrl default state: %d\n", err);
        goto cleanup2;
    }
    dev->clk = of_clk_get(pdev->dev.of_node, 0);
    if(IS_ERR(dev->clk))
    {
        err = PTR_ERR(dev->clk);
        dev_err(&pdev->dev, "failed to get clock: %d\n", err);
        goto cleanup2;
    }
    err = clk_prepare_enable(dev->clk);
    if(err)
    {
        dev_err(&pdev->dev, "failed to enable clock: %d\n", err);
        goto cleanup3;
    }
    init_waitqueue_head(&dev->irq_waitqueue);
    int irq = platform_get_irq(pdev, 0);
    if(irq <= 0)
    {
        err = irq;
        dev_err(&pdev->dev, "failed to request IRQ: %d\n", err);
        goto cleanup4;
    }
    dev->rmnet0 = alloc_netdev_mqs(sizeof(struct sipc_device*), "rmnet%d", NET_NAME_UNKNOWN, rmnet_setup, 1, 1);
    if(!dev->rmnet0 || IS_ERR(dev->rmnet0))
    {
        err = PTR_ERR(dev->rmnet0);
        dev_err(&pdev->dev, "failed to create rmnet0: %d\n", err);
        goto cleanup4;
    }
    *(struct sipc_device**)netdev_priv(dev->rmnet0) = dev;
    dev->rmnet0->mtu = 1500;
    dev->rmnet0->netdev_ops = &rmnet_ops;
    dev_net_set(dev->rmnet0, &init_net);
    if((err = register_netdev(dev->rmnet0)))
    {
        dev_err(&pdev->dev, "failed to register rmnet0: %d\n", err);
        goto cleanup5;
    }
    err = request_threaded_irq(irq, atomic_handler, threaded_handler, 0, "sipc mcu irq", dev);
    if(err)
        goto cleanup6;
    if((err = do_misc_register(&pdev->dev, &dev->umts_boot0, "umts_boot0", &boot0_fops)))
        goto cleanup7;
    if((err = do_misc_register(&pdev->dev, &dev->umts_ipc0, "umts_ipc0", &ipc0_fops)))
        goto cleanup8;
    if((err = do_misc_register(&pdev->dev, &dev->umts_rfs0, "umts_rfs0", &rfs0_fops)))
        goto cleanup9;
    dev->irq = irq;
    platform_set_drvdata(pdev, dev);
    set_state(dev, MODEM_STATE_DEFUNCT);
    return 0;
cleanup9:
    misc_deregister(&dev->umts_ipc0);
cleanup8:
    misc_deregister(&dev->umts_boot0);
cleanup7:
    free_irq(irq, dev);
cleanup6:
    unregister_netdev(dev->rmnet0);
cleanup5:
    free_netdev(dev->rmnet0);
cleanup4:
    clk_disable_unprepare(dev->clk);
cleanup3:
    clk_put(dev->clk);
cleanup2:
    iounmap(dev->ipc_mapping);
cleanup1:
    iounmap(dev->firmware_mapping);
    rx_destroy(&dev->rfs0_rx);
    rx_destroy(&dev->ipc0_rx);
    rx_destroy(&dev->boot0_rx);
    mutex_destroy(&dev->fw_mtx);
    mutex_destroy(&dev->irq_mtx);
    mutex_destroy(&dev->fmt_tx_mtx);
    mutex_destroy(&dev->raw_tx_mtx);
    return err;
}

//this code is very questionable and probably wrong
//and most platform drivers don't have remove handlers anyway
//...but the kernel does not care and still lets user unbind the device, so it's probably better to have a clean cleanup
#if 1
static int sipc_remove(struct platform_device* pdev)
{
    struct sipc_device* dev = platform_get_drvdata(pdev);
    lock_ipc(dev);
    mutex_lock(&dev->fw_mtx);
    set_state(dev, MODEM_STATE_OFFLINE);
    sipc_do_reset(dev);
    mutex_unlock(&dev->fw_mtx);
    unlock_ipc(dev);
    misc_deregister(&dev->umts_rfs0);
    misc_deregister(&dev->umts_ipc0);
    misc_deregister(&dev->umts_boot0);
    free_irq(dev->irq, dev);
    unregister_netdev(dev->rmnet0);
    free_netdev(dev->rmnet0);
    clk_disable_unprepare(dev->clk);
    clk_put(dev->clk);
    iounmap(dev->ipc_mapping);
    iounmap(dev->firmware_mapping);
    rx_destroy(&dev->rfs0_rx);
    rx_destroy(&dev->ipc0_rx);
    rx_destroy(&dev->boot0_rx);
    mutex_destroy(&dev->fw_mtx);
    mutex_destroy(&dev->irq_mtx);
    mutex_destroy(&dev->fmt_tx_mtx);
    mutex_destroy(&dev->raw_tx_mtx);
    return 0;
}
#endif

static const struct of_device_id sipc_of_match[] = {
    {
        .compatible = "samsung,ss310ap-modem",
        .data = NULL,
    },
    {},
};
MODULE_DEVICE_TABLE(of, sipc_of_match);

struct platform_driver sipc_driver = {
    .driver = {
        .name = "sipc",
        .of_match_table = of_match_ptr(sipc_of_match),
    },
    .probe = sipc_probe,
    .remove = sipc_remove,
};
module_platform_driver(sipc_driver);

MODULE_LICENSE("GPL");
